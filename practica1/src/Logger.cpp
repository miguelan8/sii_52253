#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

int main (int argc, char *argv[])
{
	int fdfifo;
	int dim=100;
	char buffer[dim];
	if (mkfifo("/tmp/tuberia", 0666)==0)
	{
		fdfifo=open("/tmp/tuberia", O_RDONLY);
		if(fdfifo==-1)
		{	
			perror("Error al abrir la tuberia");
			return 1;
		}
		while(read(fdfifo, buffer, 150)>0)
		{	
			printf(buffer);
			printf( "\n");
		}
		close(fdfifo);
		unlink("/tmp/tuberia");
		return 0;
	}
	else
	{
		perror("Error al crear la tuberia");
		return 1;
	}
}
