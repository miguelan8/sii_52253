#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <DatosMapeoMemoria.h>

int main(void)
{
	int fichero;
	DatosMapeoMemoria* pmem;
	char* mapa;
	fichero=open("/tmp/bot.txt",O_RDWR);
	mapa=(char*)mmap(NULL,sizeof(*(pmem)),PROT_WRITE|PROT_READ,MAP_SHARED,fichero,0);
	close(fichero);
	pmem=(DatosMapeoMemoria*)mapa;

	while(1)
	{ 
		float cRaqueta;			//Coordenadas del centro de la raqueta
		cRaqueta=((pmem->raqueta1.y1+pmem->raqueta1.y2)/2);
		if(cRaqueta>pmem->esfera.centro.y)
			pmem->accion=-1;
		else if(cRaqueta<pmem->esfera.centro.y)
			pmem->accion=1;
		else
			pmem->accion=0;
	}
	munmap(mapa,sizeof(*(pmem)));	
}
